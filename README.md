This is a Calculator written in Kotlin for Android and using Jetpack Compose.  This is a personal project and my first ever application written for Android.

The step-by-step process by which I built the app can be found in the issue **Construction of App**
https://gitlab.com/all-personal-projects/android-apps/calculator/-/issues/1.

The calculator contains Number[0-9], Operator[+,-,*,/], Sign[+/-], Equal[=], Decimal[.] and Clear[C] buttons.  Invalid inputs are handled by preventing the user from pressing a button when it is invalid to do so.

I am also working on other projects with other people, one of which is a full stack web application.  I will keep updating and fixing bugs as needed.

Let me know if you have any feedback!

<img src="/uploads/dde3ad3e98357dba1b77057f9631b08d/Calculator_step3.jpg" width="150" height="310">
<img src="/uploads/5a8693d1ffdadac28ea4597632095f7a/Calculator_step4.jpg" width="150" height="310">
<img src="/uploads/08329e6c7a46a4da2979d2968f76436a/Calculator_step5.jpg" width="150" height="310">

**RELEASE NOTES:**\
**Version 1.01** - 25/10/2023
- Shows a message when user tries to enter a number with more than 15 digits.  This is to prevent inaccuracy of result due to rounding/truncation because of how numbers are represented.
- Added commas for easier visualization of longer numbers.
- Changed layout.
<img src="/uploads/326067244bba0f0fc66d07f0df0ef7aa/Calculator_v1.01.jpg" width="200" height="420">
